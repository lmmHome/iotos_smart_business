import Main from '@/components/main';
// import parentView from '@/components/parent-view';

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面在切换标签后不会缓存，如果需要缓存，无需设置这个字段，而且需要设置页面组件name属性和路由配置的name一致
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */

export default [
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/500.vue')
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/404.vue')
  },
  {
    path: '/login',
    name: 'login',
    topMenuCommon: true, // router.js中添加条件：有设置topMenuCommon的且值为true的，就认为是顶部菜单共有的操作菜单！再时候显示取决于hideInMenu！
    meta: {
      title: 'Login - 登录',
      hideInMenu: true
    },
    // component: () => import('@/view/login/login.vue')
    component: resolve => require(['@/view/login/login.vue'], resolve),
  },
  {
    path: '/',
    name: '_home',
    topMenuCommon: true, // router.js中添加条件：有设置topMenuCommon的且值为true的，就认为是顶部菜单共有的操作菜单！再时候显示取决于hideInMenu！
    redirect: '/home',
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true
    },
    children: [
      {
        path: '/home',
        name: 'home',
        meta: {
          title: '首页',
          notCache: true,
          icon: 'md-home'
        },

        // component: () => import("@/view/single-page/home")
        component: () => import('@/view/gismap/index.vue')
      }
    ]
  },
  {
    path: '/message',
    name: '消息中心',
    topMenuCommon: true,
    component: Main,
    meta: {
      hideInBread: true,
      hideInMenu: false
      // access: ['aoneng1']
    },
    children: [
      {
        path: 'message_page',
        name: 'message_page',
        meta: {
          icon: '_xiaoxi1',
          title: '消息中心'
        },
        component: () => import('@/view/single-page/message/index.vue')
      }
    ]
  },
  {
    // 数组array中索引为7！！！！
    path: '/devtype',
    name: '设备类目',
    topMenuCommon: true,
    component: Main,
    meta: {
      showAlways: true,
      hideInBread: false,
      hideInMenu: false,
      icon: '_kaoqin',
      title: '设备类目'
    },
    children: [
      // //临时应付湖南热源项目演示
      // {
      //   path: "message",
      //   name: '热水系统',
      //   component: () => import("@/view/gismap/index.vue"),
      //   beforeEnter() {
      //     window.location = "/dataVusual.html"
      //   },
      //   meta: {
      //     hideInBread: false,
      //     hideInMenu: false,
      //     icon: "_kaoqin",
      //     title: '热水系统'
      //   }
      // },
    ]
  },
  {
    path: '/gis',
    name: 'GIS', // 这里不论是父菜单还是兄弟子菜单之间，name都不能重复！！另外，对于只有一级菜单没有子菜单的，这里就把子菜单放到台面作为根节点展示了！！
    topMenuCommon: true,
    meta: {
      hideInMenu: true, // top菜单隐藏对应在side菜单上！
      hideInBread: true
    },
    component: Main,
    children: [
      {
        path: 'gis',
        name: '电子地图',
        meta: {
          icon: '_kaoqin',
          title: '电子地图'
        },
        component: () => import('@/view/gismap/index.vue')
      }
    ]
  },
  {
    path: '/checkData',
    name: '实时监测', // 这里不论是父菜单还是兄弟子菜单之间，name都不能重复！！另外，对于只有一级菜单没有子菜单的，这里就把子菜单放到台面作为根节点展示了！！
    topMenuCommon: true,
    meta: {
      hideInMenu: true, // top菜单隐藏对应在side菜单上！
      hideInBread: true,
      icon: '_kaoqin',
      title: '实时监测'
    },
    component: Main,
    children: [
      {
        path: 'cloudMap',
        name: '实时云图',
        meta: {
          icon: '_ICqiaduxieqi',
          title: '实时云图'
        },
        component: () => import('@/view/checkData/cloudMap/index.vue')
      },
      {
        path: 'pointList',
        name: '数据点表',
        meta: {
          icon: '_ICqiaduxieqi',
          title: '数据点表'
        },
        component: () => import('@/view/checkData/pointList/index.vue')
      },
      {
        path: 'realData',
        name: '实时数据',
        meta: {
          icon: '_ICqiaduxieqi',
          title: '实时数据'
        },
        component: () => import('@/view/checkData/realData/index.vue')
      }
    ]
  },

  {
    path: '/alarm',
    name: '报警管理',
    topMenuCommon: true, // router.js中添加条件：有设置topMenuCommon的且值为true的，就认为是顶部菜单共有的操作菜单！再时候显示取决于hideInMenu！
    meta: {
      title: '报警管理',
      hideInMenu: true, // top菜单隐藏对应在side菜单上！
      hideInBread: true
    },
    component: Main,
    children: [
      {
        path: 'realtime',
        name: '实时记录',

        // redirect: "/401",

        meta: {
          icon: '_kaoqin',
          title: '实时记录'
        },
        component: () => import('@/view/alarm/realtime.vue')
      },
      {
        path: 'history',
        name: '历史记录',

        // redirect: "/401",

        meta: {
          icon: '_kaoqin',
          title: '历史记录'
        },
        component: () => import('@/view/alarm/history.vue')
      },
      {
        path: 'setting',
        name: '报警设置',

        // redirect: "/401",

        meta: {
          icon: '_kaoqin',
          title: '报警设置'
        },
        component: () => import('@/view/alarm/setting.vue')
      },
      {
        path: 'alarm_contact',
        name: '告警联系人',

        // redirect: "/401",

        meta: {
          icon: '_kaoqin',
          title: '告警联系人'
        },
        component: () => import('@/view/alarm/alarm_contact.vue')
      }
    ]
  },
  // 添加报警数据
  {
    path: '/alarm',
    name: '添加报警数据',
    topMenuCommon: true, // router.js中添加条件：有设置topMenuCommon的且值为true的，就认为是顶部菜单共有的操作菜单！再时候显示取决于hideInMenu！
    meta: {
      title: '添加报警数据',
      hideInMenu: true, // top菜单隐藏对应在side菜单上！
      hideInBread: true
    },
    component: Main,
    children: [
      {
        path: 'addData',
        name: '创建报警数据',
        meta: {
          icon: '_kaoqin',
          title: '创建报警数据'
        },
        component: () => import('@/view/alarm/addData.vue')
      },
      {
        path: 'editData',
        name: '修改报警数据',
        meta: {
          icon: '_kaoqin',
          title: '修改报警数据'
        },
        component: () => import('@/view/alarm/editData.vue')
      }
    ]
  }

];
