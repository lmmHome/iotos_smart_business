import Vue from "vue";
import Main from "@/components/main";
import parentView from "@/components/parent-view";
import {
  status,
  subIotos,
  closeSubs
} from "./index";

import {
  loadTable,
  initDataHub,
  saveTable
} from "@/libs/util";

export default {
  state: {
    isConnected: false,
    route2Table: loadTable(), //放到这里就是组件实例化的时候进行初始化，最常见的场景就是浏览器页面刷新！
    data: {},
    dataHub: initDataHub(),
    menu: {},
    reconnectError: false
  },
  getters: {
    // Getter 接受 state 作为其第一个参数，返回项目列表
    projectList: state => {
      var optiontmp = [{
        id: "",
        label: "",
        children: []
      }];
      for (var key in state.route2Table) {
        optiontmp[0].children.push({
          id: key,
          label: state.route2Table[key].name
        });
      }
      return optiontmp;
    }
  },
  mutations: {
    // default handler called for all methods
    //{
    // data_id: "427d"
    // data_pk: 27
    //2019.6.19 by lrq 比如这里的dev_info，设备ID是全局唯一？？要再回过头去确定一下！
    //如果全局唯一，那么“设备ID.数据点ID”确实可以唯一寻址。但是如果真的唯一了，那么会不会导致驱动复用问题，不同项目之间无缝复制是否会出问题？？？目前先按照设备地址可以全局唯一来算！！
    // dev_info: "纺大5.9栋——91e2a9ff"
    // name: "压力监测2"
    // rw: 0
    // tm: "2019-06-19 14:03:47.861268"
    // ts: 1560924227861.291
    // val: "0.0"
    // val_type: "FLOAT"
    //}
    ON_WEBSOCKET(state, rawData) {
      if (rawData.event == status.SOCKET_ONMESSAGE) {
        state.data = rawData.data;
        if (state.data.dev_info.indexOf("——") != -1) {
          let devid = state.data.dev_info.split("——")[1]
          let idtmp = devid + "." + state.data.data_id
          state.dataHub[idtmp].val = state.data.val
          if (state.dataHub[devid].status != 1)
            state.dataHub[devid].status = 1
        }
      } else {
        if (
          rawData.event == status.SOCKET_DEVSTATUS
        ) {
          state.data = rawData.data;
          if (state.data.event == 'online') {
            state.dataHub[state.data.device_id].status = 1
          } else if (state.data.event == 'offline') {
            state.dataHub[state.data.device_id].status = 0
          }
        } else if (
          rawData.event == status.SOCKET_ONERROR ||
          rawData.event == status.SOCKET_ONCLOSE
        ) {
          // Vue.prototype.$Message.error(rawData.data)
          // console.error(JSON.stringify(rawData))
        } else if (
          rawData.event == status.SOCKET_RECONNECT ||
          rawData.event == status.SOCKET_WARNING
        ) {
          // Vue.prototype.$Message.warning(rawData.data)
          // console.warn(JSON.stringify(rawData))
        } else if (rawData.event == status.SOCKET_ONOPEN) {
          // Vue.prototype.$Message.success(rawData.data)
          // console.log(JSON.stringify(rawData))
        } else if (rawData.event == status.SOCKET_CONNECTING) {
          // Vue.prototype.$Message.loading(rawData.data, 0)
          // console.warn(JSON.stringify(rawData))
        }
        // else
        //   console.log(JSON.stringify(rawData))
      }
    },

    //added by lrq 2019.6.14
    INIT_ROUTE_TABLE(state, table) {
      saveTable(table);
      //加载点表的过程就包含了数据订阅
      state.route2Table = loadTable();
      state.data = {}
      state.dataHub = initDataHub()

      // if (window.vue) {
      //   let datatmp = {
      //     path: "/message",
      //     name: obj.val.name,
      //     topMenuCommon: true,
      //     component: Main,
      //     meta: {
      //       hideInBread: false,
      //       hideInMenu: false,
      //       icon: "_kaoqin",
      //       title: obj.val.name
      //     }
      //   };
      //   // window.vue.$router.options.routes[7].children.push(datatmp)
      //   // window.vue.$router.addRoutes(window.vue.$router.options.routes)
      // }
    },

    //add by lrq
    CLEAR(state) {
      state.data = {}
      state.route2Table = {}
      // state.dataHub = {}     //在realData中用到了这个对象渲染，所以登出情空，就会导致渲退出报错。这里对route2Table清空就好了，因为dataHub的初始化是依赖于前者内容的！！
      closeSubs()
    },
  },

  actions: {
    sendData: function (context, data) {
      var jsonData = Vue.prototype.$api.convertJson(data);
      Vue.prototype.$socket.send(JSON.stringify(jsonData));
    },
    on_websocket({
      commit
    }, rawData) {
      commit("ON_WEBSOCKET", rawData);
    },
    initRouteTable({
      commit
    }, obj) {
      commit("INIT_ROUTE_TABLE", obj);
    },
    clear({
      commit
    }) {
      commit("CLEAR");
    }
  }
};
