import axios from '@/libs/api.request'

export const getTableData = () => {
  return axios.request({
    url: 'get_table_data',
    method: 'get'
  })
}

export const getDragList = () => {
  return axios.request({
    url: 'get_drag_list',
    method: 'get'
  })
}

export const errorReq = () => {
  return axios.request({
    url: 'error_url',
    method: 'post'
  })
}
// updateRouteTable
export const saveErrorLogger = info => {
  return axios.request({
    url: 'save_error_logger',
    data: info,
    method: 'post'
  })
}

export const uploadImg = formData => {
  return axios.request({
    url: 'image/upload',
    data: formData
  })
}


//added by lrq 2019.5.23
export const convertJson = (data) => {
  if (typeof (data) == 'object') {
    return data
  } else {
    return JSON.parse(data)
  }
}

export const log = (jsonData) => console.log(JSON.stringify(convertJson(jsonData), undefined, 2))
export const warn = (jsonData) => console.warn(JSON.stringify(convertJson(jsonData), undefined, 2))
export const error = (jsonData) => console.error(JSON.stringify(convertJson(jsonData), undefined, 2))

export const getTableDetail = uuid => {
  return axios.request({
    url: 'api/protocal/node?node_uuid=' + uuid,
    method: 'get'
  })
}

export const sentSwitch = ({
  dev_id_list,
  data_id,
  value
}) => {
  let param = new URLSearchParams()
  param.append('dev_id_list', dev_id_list)
  param.append('data_id', data_id)
  param.append('value', value)
  return axios.request({
    url: 'backstage/setdata/',
    data: param,
    method: 'post',
  })
}


//告警
//获取告警
export const getWarnData = login_user_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/remind/?login_user_id=' + login_user_id,
    method: 'get'
  })
}

// 获取已发出告警记录
// POST：http://sys.iotos.net.cn/backstage/remind/smsinfo/
export const getWarnRecode = (ionode_uuid) => {
  var formData = new FormData();
  formData.append('ionode_uuid', ionode_uuid)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/remind/smsinfo/',
    method: 'post',
    data: formData
  })
}

// 获取告警通知人
// GET：http://sys.iotos.net.cn/backstage/remind_user/
export const getInformMan = (login_user_id) => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/remind_user/?login_user_id=' + login_user_id,
    method: 'get'
  })
}

// 创建告警通知人
// POST：http://sys.iotos.net.cn/backstage/remind_user/ 请求参数：
export const createInformMan = (data) => {
  var formData = new FormData();
  formData.append('username', data.username)
  formData.append('phone', data.phone)
  formData.append('email', data.email)
  formData.append('login_user_id', data.login_user_id)
  formData.append('desc', data.desc)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/remind_user/',
    method: 'post',
    data: formData
  })
}
// 删除告警通知人
// DELETE： http://sys.iotos.net.cn/backstage/remind_user/<:remind_user_id>/
export const delInformMan = remind_user_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/remind_user/' + remind_user_id + '/',
    method: 'DELETE'
  })
}

// 编辑告警通知人
// POST：http://sys.iotos.net.cn/backstage/remind_user/<:remind_user_id>/
export const editInformMan = (data) => {
  var formData = new FormData();
  formData.append('username', data.username)
  formData.append('phone', data.phone)
  formData.append('email', data.email)
  formData.append('desc', data.desc)
  formData.append('login_user_id', data.login_user_id)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/remind_user/' + data.remind_user_id + '/',
    method: 'post',
    data: formData
  })
}

// 获取通知用户
export const getInformAccount = () => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/account/',
    method: 'get',
  })
}


//創建警告

export const createWarn = (data) => {
  var formData = new FormData();
  formData.append('name', data.name)
  formData.append('grade', data.grade)
  formData.append('condition', data.condition)
  formData.append('inform_time', data.inform_time)
  formData.append('inform_type', data.inform_type)
  formData.append('user_ids', data.user_ids)
  formData.append('is_open', data.is_open)
  formData.append('remind_time', data.remind_time)
  formData.append('rule', data.rule)
  formData.append('login_user_id', data.login_user_id)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/remind/',
    method: 'post',
    data: formData
  })
}

//编辑告警
export const editWarnRule = (data) => {
  var formData = new FormData();
  formData.append('name', data.name)
  formData.append('grade', data.grade)
  formData.append('condition', data.condition)
  formData.append('inform_time', data.inform_time)
  formData.append('inform_type', data.inform_type)
  formData.append('user_ids', data.user_ids)
  formData.append('is_open', data.is_open)
  formData.append('remind_time', data.remind_time)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_remind/' + data.id + '/',
    method: 'post',
    data: formData
  })
}

//删除告警
export const delWarn = id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_remind/' + id + '/',
    method: 'DELETE'
  })
}


//开启/关闭告警：
// POST：http://sys.iotos.net.cn/backstage/remind/<:id>/

export const warnControl = ({ id, is_open }) => {
  var formData = new FormData();
  formData.append('is_open', is_open)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/remind/' + id + '/',
    data: formData,
    method: 'post',
  })
}


// 获取告警详情（带子数据）
export const getWarnMoreDetails = id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/remind/' + id + '/',
    method: 'get',

  })
}


//	获取告警详情
export const getWarnDetails = id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_remind/' + id + '/',
    method: 'get',
  })
}

//获取告警设备详情
export const getWarnEquipmentDetails = remind_device_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_remind_device/' + remind_device_id + '/',
    method: 'get',
  })
}

//删除告警设备
export const delWarnEquipment = remind_device_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_remind_device/' + remind_device_id + '/',
    method: 'DELETE'

  })
}
//编辑告警设备
// http://sys.iotos.net.cn/backstage/edit_remind_device/<:remind_device_id>/
export const editWarnEquipment = (data) => {
  var formData = new FormData();
  formData.append('title', data.title)
  formData.append('ionode_id', data.ionode_id)
  formData.append('device_id', data.device_id)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_remind_device/' + data.remind_device_id + '/',
    method: 'post',
    data: formData
  })
}
//获取告警监控内容
export const getWarnMonitorContent = rule_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_remind_rule/' + rule_id + '/',
    method: 'post',

  })
}
//删除告警监控内容
export const delWarnMonitorContent = rule_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_remind_rule/' + rule_id + '/',
    method: 'DELETE'

  })
}

// 创建告警监控内容
// POST：http://sys.iotos.net.cn/backstage/create_remind_rule/
export const createWarnMonitorContent = data => {
  var formData = new FormData();
  formData.append('id', data.id)
  formData.append('device_id', data.device_id)
  formData.append('ionode_id', data.ionode_id)
  formData.append('title', data.title)
  formData.append('data_id', data.data_id)
  formData.append('operator', data.operator)
  formData.append('second', data.second)
  formData.append('unit', data.unit)
  formData.append('value', data.value)
  formData.append('is_open', data.is_open)
  formData.append('logic', data.logic)
  formData.append('logic_info', data.logic_info)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/create_remind_rule/',
    method: 'post',
    data: formData
  })
}


//编辑告警监控内容
export const editWarnMonitorContent = data => {
  var formData = new FormData();
  formData.append('title', data.title)
  formData.append('ionode_id', data.ionode_id)
  formData.append('device_id', data.device_id)
  formData.append('remind_device_id', data.remind_device_id)
  formData.append('data_id', data.data_id)
  formData.append('second', data.second)
  formData.append('valueType', data.valueType)
  formData.append('operator', data.operator)
  formData.append('value', data.value)
  formData.append('unit', data.unit)
  formData.append('logic', data.logic)
  formData.append('logic_info', data.logic_info)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_remind_rule/' + data.rule_id +'/',
    method: 'post',
    data: formData
  })
}
//获取告警处理设备详情
export const getWarnHandleEquipment = remind_dispose_device_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_dispose_device/' + remind_dispose_device_id + '/',
    method: 'get',
  })
}
//删除告警处理设备
export const delWarnHandleEquipment = remind_dispose_device_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_dispose_device/' + remind_dispose_device_id + '/',
    method: 'DELETE',
  })
}
//编辑告警处理设备
// http://sys.iotos.net.cn/backstage/edit_dispose_device/<:remind_dispose_device_id>/
export const editWarnHandleEquipment = (data) => {
  var formData = new FormData();
  formData.append('ionode_id', data.ionode_id)
  formData.append('device_id', data.device_id)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_dispose_device/' + data.remind_dispose_device_id + '/',
    method: 'post',
    data: data
  })
}
//获取告警处理监控内容详情
export const getWarnMonitorDetails = dispose_rule_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_dispose_rule/' + dispose_rule_id + '/',
    method: 'get',
  })
}
//删除告警处理监控内容
export const delWarnMonitorDetails = dispose_rule_id => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_dispose_rule/' + dispose_rule_id + '/',
    method: 'DELETE',
  })
}
//编辑告警处理监控内容
export const editWarnMonitorDetails = (data) => {
  var formData = new FormData();
  formData.append('ionode_id', data.ionode_id)
  formData.append('device_id', data.device_id)
  formData.append('remind_dispose_device_id', data.remind_dispose_device_id)
  formData.append('data_id', data.data_id)
  formData.append('value_type', data.valueType)
  formData.append('value', data.value)
  formData.append('unit', data.unit)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/edit_dispose_rule/' + data.dispose_rule_id + '/',
    method: 'post',
    data: formData
  })
}

// 创建告警处理监控内容
// POST：http://sys.iotos.net.cn/backstage/create_dispose_rule/
export const createWarnMonitorDetails = data => {
  var formData = new FormData();
  formData.append('id', data.id)
  formData.append('device_id', data.device_id)
  formData.append('ionode_id', data.ionode_id)
  formData.append('dispose_info', data.dispose_info)
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/create_dispose_rule/',
    method: 'post',
    data: formData
  })
}


// ### 获取用户网关
// GET：http://sys.iotos.net.cn/backstage/ionode/
export const getUserIonode = (login_user_id) => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/ionode/?login_user_id=' + login_user_id,
    method: 'get',
    // data:formData
  })
}


// ### 获取用户设备
// GET：http://sys.iotos.net.cn/backstage/device/
export const getUserEquipment = (login_user_id, ionode_id) => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/device?login_user_id=' + login_user_id + '&ionode_id=' + ionode_id,
    method: 'get',
    // data:formData
  })
}
// ### 获取用户数据点
// GET：http://sys.iotos.net.cn/backstage/data/
export const getUserData = (login_user_id, device_id) => {
  return axios.request({
    url: 'http://sys.iotos.net.cn/backstage/data/?login_user_id=' + login_user_id + '&device_id=' + device_id,
    method: 'get',
  })
}