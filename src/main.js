// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import store from "./store";
import iView from "iview";
import elementUI from "element-ui"
import locale from '@/locale/lang/en'
// import enLocale from 'element-ui/lib/locale/lang/en'
// import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
import 'element-ui/lib/theme-chalk/index.css';
import i18n from "@/locale";
import config from "@/config";
// import echarts from "../node_modules/echarts";
import importDirective from "@/directive";
import installPlugin from "@/plugin";
import "./index.less";
import "@/assets/icons/iconfont.css";
import TreeTable from "tree-table-vue";

import AMap from "vue-amap";
Vue.use(AMap);
// 初始化vue-amap
AMap.initAMapApiLoader({
  // 申请的高德key
  key: "5fc48c6422c3c51809f7de59d775ce9f",
  // 插件集合
  plugin: [
    "AMap.Autocomplete",
    "AMap.PlaceSearch",
    "AMap.Scale",
    "AMap.OverView",
    "AMap.ToolBar",
    "AMap.MapType",
    "AMap.PolyEditor",
    "AMap.CircleEditor",
    "AMap.Geolocation"
  ]
});

// 实际打包时应该不引入mock
/* eslint-disable */
// if (process.env.NODE_ENV !== "production") require("@/mock");

Vue.use(iView, {
  i18n: function(path, options) {
    let value = i18n.t(path, options)
    if (value !== null && value !== undefined) {
      return value
    }
    return ''
  }
});

Vue.use(elementUI, {
  i18n: function (path, options) {
  // let value = i18n.t(path, options)
  // if (value !== null && value !== undefined) {
  //   return value
  // }
  // return ''
  }
})

Vue.use(TreeTable);
/**
 * @description 注册admin内置插件
 */
installPlugin(Vue);
/**
 * @description 生产环境关掉提示
 */
Vue.config.productionTip = false;
/**
 * @description 全局注册应用配置
 */
Vue.prototype.$config = config;
/**
 * 注册指令
 */
importDirective(Vue);

//注册自定义通用函数到vue全局属性中
import * as Api from "@/api/data";
Vue.prototype.$api = Api;

import subIotos from "./store/module/iotos-websocket/index";
Vue.prototype.$subIotos = subIotos;

//edit by lrq amap初始化完毕后调用的函数，通过错误日志发现是AMap.initAMapApiLoaderd的回调函数是amapInitComponent！！
//而每个vue-amap实例使用了plugin插件，必须要初始化，而每次初始化都会触发调用一次callback，所以将vue全局实例化放到地图初始化完毕开始，同时仅在第一次进行，避免多次实例化vue全局实例！
var main = null;
window.amapInitComponent = () => {
  /* eslint-disable no-new */
  if (!main) {
    main = new Vue({
      el: "#app",
      router,
      i18n,
      store,
      render: h => {
        return h(App)
      }
    });
    window.vue = main
  }
}
